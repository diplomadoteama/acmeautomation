Feature: Eventuality

  Scenario: Eventuality - Accidents - Verificar que el usuario puede registrar un nuevo accidente
    Given Ingresar al SSI Acme
    When Click en el link Incident and Accident del header
    And Click en la opcion Incident and Accident
    And Click en el boton +
    And Elegir Accidente en el combo box de Type Event
    And Llenar el formulario con valores validos
      | Date of event      | 6/28/2018       |
      | Injury Type        | Escoriaciones   |
      | Injury Part        | Region craneana |
      | Leave a description| Test desc.      |
      | Employee ID        | Miguel Lopez    |
    And Click en el boton save #Hacer click en el boton save
    And Buscar el registro con la descripcion "Test desc."
    Then Verificar el nuevo registro de un "Accidente"

  #Scenario: Eventuality - Accidents - Verificar que el usuario puede eliminar un accidente
  #  Given Ingresar al SSI Acme
  #  When Click en el link Incident and Accident del header
  #  And Click en la opcion Incident and Accident
  #  And Elegir un accident de la lista
  #  And Click en el icono de Delete del registro seleccionado
  #  And Click en el boton Delete
  #  And Filtrar por el campo nombre "Audit Sample"
  #  Then Verificar que el registro se elimino "No results"
