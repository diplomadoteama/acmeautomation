Feature: CategoryFeature
  Scenario: CreateCategory
    Given pagina principal acme
    When ir a tab Category
    And Ingresar a tabla categories
    And ingresar al dialogo add category
    And llegar datos category
      | code        | 0005     |
      | Name | testName |
      | Description | testDescription |
    And enviar formulario

  Scenario: DeleteCategory
    Given pagina principal acme
    When ir a tab Category
    And Ingresar a tabla categories
    And delete category row
    And delete button

  Scenario: updateCategory
    Given pagina principal acme
    When ir a tab Category
    And Ingresar a tabla categories
    And modif category row
    And cambiar datos
      | code        | 0006    |
      | Name | modifN |
      | Description | ModifDe |
    And save button