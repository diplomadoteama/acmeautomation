Feature: Area

  Scenario: Area insertar nuevo registro
    Given Accedo a la pagina
    When Navego al tab "Employee"
    And Ingreso al tab "Areas"
    And Hacer click en add
    And Lleno datos del Area form
      | code | Dh001   |
      | name | Quality |
    And Hacer click en save
    And Filtrar por nombre "Quality"
    Then Verificar registro insertado "Quality"

  Scenario: Editar Area
    Given Accedo a la pagina
    When Navego al tab "Employee"
    And Ingreso al tab "Areas"
    And Filtrar por nombre "Quality"
    And Hacer click en registro a editar
    And Modificar datos del registro
      | code | DHR001   |
      | name | Modified |
    And Hacer click en save
    And Filtrar por nombre "Modified"
    Then Verificar registro modificado "Modified"

  Scenario: Eliminar Area
    Given Accedo a la pagina
    When Navego al tab "Employee"
    And Ingreso al tab "Areas"
    And Filtrar por nombre "Modified"
    And Hacer click en el registro a eliminar
    And Hacer click en Delete
    And Filtrar por nombre "Modified"
    Then Verificar que fue eliminado "No results"
