Feature: Contract

  Scenario: Registrar un contrato
    Given Acceder a la pagina Acme
    When Realizar click al tab "Contrat"
    And Hacer click en la opcion "Register Contract"
    And Llenar los campos del Form Contract
      | codigo          | PV_EO_0230    |
      | tipoContrato    | Destajo       |
      | proyecto        | vivienda      |
      | fechaInicio     | 8/5/2018      |
      | fechaFin        | 8/20/2018     |
      | posicionTrabajo | OPERATIVE     |
      | montoPago       | Contractor    |
      | tipoPago        | effective     |
      | empleado        | John          |
    And Hacer click en en el boton save
    Then Verificar el contract creado "PV_EO_0230"

  Scenario: Verificar lista de Empleados mostrados en el formulario Contrato
    Given Acceder a la pagina
    When Navegar al tab Employee
    And Ingresar al tab Employees
    When Filtrar por name "Test"
    And verificar nombre empleado.
    #And Cerrar pagina

  Scenario:  Verificar lista de Proyectos mostrados en el formulario Contrato
    Given Acceder a la pagina
    When Ingresar al tab "Proyects"
    And filtrar por nombre de proyecto
    And verificar nombre Proyecto.


