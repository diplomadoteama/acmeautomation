Feature: Project

  Scenario: Project insertar nuevo projecto
    Given Acceder a la pagina acme
    When Ir al tab de "Project"
    And Hacer click en el submenu projects "Projects"
    And Hacer click en boton para anadir project
    And Llenar datos en el project form
      | projectname | project Automation   |
      | description | Project Description Test |
      | date_start  | 7/25/2018                |
      | date_end    | 8/25/2018                |
   And Hacer click en save project
   And Filtrar por nombre de projecto "project Automation"
   Then Verificar el projecto creado "project Automation"
