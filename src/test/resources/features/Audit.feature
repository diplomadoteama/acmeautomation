Feature: Audit

  Scenario: Audit insertar nuevo registro
    Given Acceder a la pagina de ACME
    When Navegar al Menu "Audits"
    And Ingresar al SubMenu "Audit"
    And Hacer click en el boton add
    And Llenar los campos del form Audit
      | Audit Name      | test Audit     |
      | Audit Code      | test-001       |
      | Audit Scope     | test scope     |
      | Audit Objective | test objective |
      | Audit Criteria  | test criteria   |
    And Hacer click en el boton save
    And Filtrar por el campo nombre "test Audit"
    Then Verificar registro agregado "test Audit"

  Scenario: Editar Auditoria
    Given Acceder a la pagina de ACME
    When Navegar al Menu "Audits"
    And Ingresar al SubMenu "Audit"
    And Filtrar por el campo nombre "test Audit"
    And Hacer click en editar
    And Modificar datos de la Auditoria
      | Audit Name | Audit Sample   |
      | Audit Code | test-002 |
      | Audit Type | INTERNAL |
      | Audit Periodicity | ANNUAL |
    And Hacer click en el boton save
    And Filtrar por el campo nombre "Audit Sample"
    Then Verificar correcta modificacion "Audit Sample"


  Scenario: Eliminar Auditoria
    Given Acceder a la pagina de ACME
    When Navegar al Menu "Audits"
    And Ingresar al SubMenu "Audit"
    And Filtrar por el campo nombre "Audit Sample"
    And Hacer click en la auditoria a eliminar
    And Confirmar haciendo click en el boton Delete

