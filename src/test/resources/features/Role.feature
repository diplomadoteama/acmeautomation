Feature: Role

  Scenario: Role insertar nuevo registro
    Given Accedo a la pagina role
    When Navego al tab role "Employee"
    And Ingreso al tab para "Roles"
    And Hacer click en add role
    And Lleno datos del Role form
      | code        | ROLE001     |
      | description | ExampleRole |
    And Hacer click en save de Role
    And Filtrar por descripcion del Role "ExampleRole"
    Then Verificar registro insertado en Role "ExampleRole"

  Scenario: Editar Role
    Given Accedo a la pagina role
    When Navego al tab role "Employee"
    And Ingreso al tab para "Roles"
    And Filtrar por descripcion del Role "ExampleRole"
    And Hacer click en registro a editar de Role
    And Modificar datos del registro de Role
      | code        | DHR002       |
      | description | ModifiedRole |
    And Hacer click en save de Role
    And Filtrar por descripcion del Role "ModifiedRole"
    Then Verificar registro modificado en Role "ModifiedRole"

  Scenario: Eliminar Role
    Given Accedo a la pagina role
    When Navego al tab role "Employee"
    And Ingreso al tab para "Roles"
    And Filtrar por descripcion del Role "ModifiedRole"
    And Hacer click en el registro a eliminar de Roles
    And Hacer click en Delete de Roles
#    And Refrescar ventana
#    And Filtrar por descripcion del Role "ModifiedRole"
#    Then Verificar que fue eliminado "No results"