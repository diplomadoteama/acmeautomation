Feature: Employee

  Scenario:  Filter Employee
    Given Acceder a la pagina
    When Navegar al tab Employee
    And Ingresar al tab Employees
    And Filtrar por name "Miguel"

  Scenario: Add New Employee
    Given Acceder a la pagina
    When Navegar al tab Employee
    And Ingresar al tab Employees
    When Hacer click en Add New Employee
    And Llenar los campos del Form Employee
      | firstName    | Test          |
      | lastName     | Test          |
      | address      | Test Address  |
      | phone        | 1234567890    |
      | email        | test@test.com |
      | dni          | 4507569       |
      | job Position | Contractor    |
    And Hacer click en en el boton save
    And Filtrar por name "Test"

  Scenario:  Delete Employee
    Given Acceder a la pagina
    When Navegar al tab Employee
    And Ingresar al tab Employees
    When Filtrar por name "Test"
    And Hacer click en el icono de borrar Employee
    And Hacer click en el boton delete
    And Filtrar por name "Test"
    #And Cerrar pagina

  Scenario:  Sort Employee
    Given Acceder a la pagina
    When Navegar al tab Employee
    And Ingresar al tab Employees
    And Hacer click en el header First Name
    And Hacer click en el header First Name


