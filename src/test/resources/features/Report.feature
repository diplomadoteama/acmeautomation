Feature: Report

  Scenario: Reporte por nombres de empleado que cominezan con la letra M
    Given Acceder a la pagina ACME
    When Ingresar al Menu "Report"
    And Ingresar al SubMenu de "Graphic Reports"
    And Llenar employee field
      | employee | M   |
   And Hacer click en boton Load