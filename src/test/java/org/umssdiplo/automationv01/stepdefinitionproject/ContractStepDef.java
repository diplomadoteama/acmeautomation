package org.umssdiplo.automationv01.stepdefinitionproject;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.umssdiplo.automationv01.core.managepage.Contract.Contract;
import org.umssdiplo.automationv01.core.managepage.Home.Header;
import org.umssdiplo.automationv01.core.utils.LoadPage;

import javax.xml.crypto.Data;
import java.util.Map;

/**
 * Description the this class ContractStepDef
 *
 * @author: la bebe bellota
 * @version: 12/08/2018
 */
public class ContractStepDef {
    private Contract contract;
    private Header  header;

    @Given("^Acceder a la pagina Acme$")
    public void contractPageIsLoaded() throws Throwable
    {
        contract = LoadPage.contractPage();
        header = LoadPage.headerPage();
    }

    @When("^Realizar click al tab \"([^\"]*)\"$")
    public void navego_al_tab_contract() throws Throwable
    {
        header.goToAllCoursesTab();
    }

    @When("^Hacer click en la opcion \"([^\"]*)\"$")
    public void ingreso_al_tab_contract() throws Throwable
    {
        header.registerContractOpClick();
    }

    @When("^Llenar los campos del Form Contract$")
    public void lleno_datos_del_Contract_form(DataTable datos) throws Throwable{
        Map<String, String> tablaDatos = datos.asMap(String.class, String.class);
        contract.setCodigoTxt(tablaDatos.get("codigo"));
        contract.tipoElementListClick(tablaDatos.get("tipoContrato"));
        contract.proyectoElementListClick(tablaDatos.get("proyecto"));
        contract.setFechaInicio(tablaDatos.get("fechaInicio"));
        contract.setFechaFin(tablaDatos.get("fechaFin"));
        contract.posicionElementListClick(tablaDatos.get("posicionTrabajo"));
        contract.setMontoPago(tablaDatos.get("montoPago"));
        contract.tipoPagoElementListClick(tablaDatos.get("tipoPago"));
        contract.setEmpleado(tablaDatos.get("empleado"));
    }

    @When("^Hacer click en save$")
    public void hacer_click_en_save() throws Throwable
    {
        contract.saveBtnClick();
    }


    @Then("^Verificar el contract creado \"([^\"]*)\"$")
    public void verificar_que_fue_insertado(String record) throws Throwable
    {
        contract.verifyRecord(record);
    }


}
