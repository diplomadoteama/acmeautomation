package org.umssdiplo.automationv01.stepdefinitionproject;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.umssdiplo.automationv01.core.managepage.Eventuality.Eventuality;
import org.umssdiplo.automationv01.core.utils.LoadPage;

public class EventualityStepDef {

   private Eventuality eventuality;

    @Given("^Ingresar al SSI Acme$")
    public void ingresar_al_SSI_Acme(){
        eventuality = LoadPage.eventualityPage();
    }

    @When("^Click en el link Incident and Accident del header$")
    public void click_en_el_link_Incident_and_Accident_del_header(){
        eventuality.setEventualities();
    }

    @When("^Click en la opcion Incident and Accident$")
    public void click_en_la_opcion_Incident_and_Accident(){
        eventuality.setEventuality();
    }

    @When("^Click en el boton \\+$")
    public void click_en_el_boton() {
        eventuality.AddNewEventuality();
    }

    @When("^Elegir Accidente en el combo box de Type Event$")
    public void elegir_Accidente_en_el_combo_box_de_Type_Event() {
        eventuality.SelectAccident();
    }

    @When("^Llenar el formulario con valores validos$")
    public void llenar_el_formulario_con_valores_validos(DataTable datos){
        eventuality.FillOutData(datos);
    }

    @When("^Click en el boton save #Hacer click en el boton save$")
    public void click_en_el_boton_save_Hacer_click_en_el_boton_save() {
        eventuality.onClickSave();
    }

    @When("^Buscar el registro con la descripcion \"([^\"]*)\"$")
    public void buscar_el_registro_con_la_descripcion(String valor) {
        eventuality.filtrarTextBox(valor);
    }

    @Then("^Verificar el nuevo registro de un \"([^\"]*)\"$")
    public void verificar_el_nuevo_registro_de_un(String record) {
        eventuality.verifyRecord(record);
    }
}
