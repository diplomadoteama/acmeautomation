package org.umssdiplo.automationv01.stepdefinitionproject;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.umssdiplo.automationv01.core.managepage.Category.Category;
import org.umssdiplo.automationv01.core.utils.LoadPage;

public class CategoryStepDef
{
     Category category;



    @Given("^pagina principal acme$")
    public void paginaPrincipalAcme() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        category = LoadPage.CategoryPage();
    }

    @When("^ir a tab Category$")
    public void irATabCategory() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        category.setCategory();
    }

    @And("^Ingresar a tabla categories$")
    public void ingresarATablaCategories() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        category.setTabCategories();
    }


    @And("^ingresar al dialogo add category$")
    public void ingresarAlDialogoAddCategory() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();
        category.AddNewCategory();
    }

    @And("^llegar datos category$")
    public void llegarDatosCategory(DataTable datos) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();
        category.InsertCategory(datos);
    }

    @And("^enviar formulario$")
    public void enviarFormulario() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        category.sendForm();
    }
    //DELETE CATEGORY
    @And("^delete category row$")
    public void deleteCategoryRow() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();
        category.deleteCategory();
    }

    @And("^delete button$")
    public void deleteButton() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();
        category.deleteCategoryButton();
    }

    @And("^modif category row$")
    public void modifCategoryRow() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();
        category.categoryDialogEdit();
    }

    @And("^cambiar datos$")
    public void cambiarDatos(DataTable datos) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();
        category.cambiarDatosForm(datos);
    }

    @And("^save button$")
    public void saveButton() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();
        category.saveUpdate();
    }

}
