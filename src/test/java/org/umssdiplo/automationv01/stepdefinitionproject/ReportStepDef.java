//---------------
package org.umssdiplo.automationv01.stepdefinitionproject;

//import browserManager.BrowserManager;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.umssdiplo.automationv01.core.managepage.Report.Report;
import org.umssdiplo.automationv01.core.utils.CommonEvents;
import org.umssdiplo.automationv01.core.utils.LoadPage;

import java.util.List;

public class ReportStepDef
{
    private Report report;

    @Given("^Acceder a la pagina ACME$")
    public void areaPageIsLoaded() throws Throwable
    {
        report = LoadPage.reportPage();
    }

    @When("^Ingresar al Menu \"([^\"]*)\"$")
    public void navego_al_tab(String tabNavegar) throws Throwable
    {
        report.setReport(tabNavegar);
    }

    @When("^Ingresar al SubMenu de \"([^\"]*)\"$")
    public void ingreso_al_tab(String areaT) throws Throwable
    {
        report.setGraphicReport(areaT);
    }


    @When("^Llenar employee field$")
    public void llenar_employee_field(DataTable datos) throws Throwable
    {
        report.InsertDatos(datos);
    }

    @When("^Hacer click en boton Load$")
    public void hacer_click_en_load() throws Throwable
    {
        report.onClickLoad();
    }



}