package org.umssdiplo.automationv01.stepdefinitionproject;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.umssdiplo.automationv01.core.managepage.Employee.Employee;
import org.umssdiplo.automationv01.core.utils.LoadPage;

public class EmployeeStepDef
{
    private Employee employee;

    @Given("^Acceder a la pagina$")
    public void employeePageIsLoaded()
    {
        employee = LoadPage.employeePage();
    }

    @When("^Navegar al tab Employee$")
    public void navegar_al_tab_Employee()
    {
        employee.setEmployee();
    }

    @When("^Ingresar al tab Employees$")
    public void ingresar_al_tab_Employees()
    {
        employee.setTabEmployees();
    }

    @When("^Filtrar por name \"([^\"]*)\"$")
    public void filtrar_por_name(String value)
    {
        employee.filterTextBox(value);
        employee.waitForNextAction();
    }

    @When("^Cerrar pagina$")
    public void cerrar_pagina()
    {
        employee.closeWindow();
    }

    @When("^Hacer click en Add New Employee$")
    public void hacer_click_en_Add_New_Employee()
    {
        employee.AddNewEmployee();
    }

    @When("^Llenar los campos del Form Employee$")
    public void llenar_los_campos_del_Form_Employee(DataTable data)
    {
        employee.InsertDatos(data);
    }

    @When("^Hacer click en en el boton save$")
    public void hacer_click_en_en_el_boton_save()
    {
        employee.waitForNextAction();
        employee.onClickSave();
    }

    @When("^Hacer click en el icono de borrar Employee$")
    public void hacer_click_en_el_icono_de_borrar_Employee()
    {
        employee.deleteEmployeeModal();
        employee.waitForNextAction();
    }

    @When("^Hacer click en el boton delete$")
    public void hacer_click_en_el_boton_delete()
    {
        employee.deleteEmployeeButton();
        employee.waitForNextAction();
    }

    @When("^Hacer click en el header First Name$")
    public void hacer_click_en_el_header_First_Name()
    {
        employee.sortFirstName();
        employee.waitForNextAction();
    }

}
