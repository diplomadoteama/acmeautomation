//--------Marvin Dickson Mendia Calizaya-------
package org.umssdiplo.automationv01.stepdefinitionproject;

//import browserManager.BrowserManager;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.umssdiplo.automationv01.core.managepage.Area.Area;
import org.umssdiplo.automationv01.core.utils.LoadPage;

public class AreaStepDef
{
    private Area area;

    @Given("^Accedo a la pagina$")
    public void areaPageIsLoaded() throws Throwable
    {
        area = LoadPage.areaPage();
    }

    @When("^Navego al tab \"([^\"]*)\"$")
    public void navego_al_tab(String tabNavegar) throws Throwable
    {
        area.setEmployee(tabNavegar);
    }

    @When("^Ingreso al tab \"([^\"]*)\"$")
    public void ingreso_al_tab(String areaT) throws Throwable
    {
        area.setArea(areaT);
        //CommonEvents.clickButton(ManageDriver.getInstance().getWebDriver().findElement(By.xpath("//button[contains(text(),'Areas')]")));
        //CommonEvents.jsClickElement(ManageDriver.getInstance().getWebDriver().findElement(By.xpath("//button[contains(text(),'Areas')]")));
    }

    @When("^Hacer click en add$")
    public void hacer_click_en_add() throws Throwable
    {
        area.AddNewArea();
    }

    @When("^Lleno datos del Area form$")
    public void lleno_datos_del_Area_form(DataTable datos) throws Throwable
    {
        area.InsertDatos(datos);
    }

    @When("^Hacer click en save$")
    public void hacer_click_en_save() throws Throwable
    {
        area.onClickSave();
    }

    @When("^Filtrar por nombre \"([^\"]*)\"$")
    public void filtrar_por_nombre(String valor) throws Throwable
    {
        area.filtrarTextBox(valor);
    }

    @Then("^Verificar registro insertado \"([^\"]*)\"$")
    public void verificar_que_fue_insertado(String record) throws Throwable
    {
        area.verifyRecord(record);
    }

    /*Escenario Edicion*/
    @When("^Hacer click en registro a editar$")
    public void hacer_click_en_registro_a_editar() throws Throwable
    {
        area.onClickEdit();
    }

    @When("^Modificar datos del registro$")
    public void modificar_datos_del_registro(DataTable datos) throws Throwable
    {
        area.modifiedDatos(datos);
    }

    @Then("^Verificar registro modificado \"([^\"]*)\"$")
    public void verificar_que_fue_Modificado(String record) throws Throwable
    {
        area.verifyRecord(record);
    }

    /*Escenario Eliminar*/
    @When("^Hacer click en el registro a eliminar$")
    public void hacer_click_en_el_registro_a_eliminar() throws Throwable
    {
        area.onClickDelete();
    }

    @When("^Hacer click en Delete$")
    public void hacer_click_en_delete() throws Throwable
    {
        area.onButtonDelete();
    }

    @Then("^Verificar que fue eliminado \"([^\"]*)\"$")
    public void Verificar_que_fue_eliminado(String result) throws Throwable
    {
        area.NoResult(result);
    }
}