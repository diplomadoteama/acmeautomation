//--------Alfredo Colque Callata-------
package org.umssdiplo.automationv01.stepdefinitionproject;

//import browserManager.BrowserManager;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.umssdiplo.automationv01.core.managepage.Audit.Audit;
import org.umssdiplo.automationv01.core.utils.LoadPage;

public class AuditStepDef
{
    private Audit audit;

    @Given("^Acceder a la pagina de ACME$")
    public void auditPageIsLoaded()
    {
        audit = LoadPage.auditPage();
    }

    @When("^Navegar al Menu \"([^\"]*)\"$")
    public void navego_al_tab(String tabAudits) throws Throwable
    {
        audit.setAudits(tabAudits);
    }

    @When("^Ingresar al SubMenu \"([^\"]*)\"$")
    public void ingreso_al_tab(String tabAudit) throws Throwable
    {
        audit.setAudit(tabAudit);
    }

    @When("^Hacer click en el boton add$")
    public void hacer_click_en_add() throws Throwable
    {
        audit.AddNewAudit();
    }
    @When("^Llenar los campos del form Audit$")
    public void lleno_datos_del_Area_form(DataTable datos) throws Throwable
    {
        audit.InsertDatos(datos);
    }

    @When("^Hacer click en el boton save$")
    public void hacer_click_en_save() throws Throwable
    {
        audit.onClickSave();
    }

    @When("^Filtrar por el campo nombre \"([^\"]*)\"$")
    public void filtrar_por_nombre(String valor) throws Throwable
    {
        audit.filtrarTextBox(valor);
    }

    @Then("^Verificar registro agregado \"([^\"]*)\"$")
    public void verificar_que_fue_insertado(String record) throws Throwable
    {
        audit.verifyRecord(record);
    }

    /*Escenario Editar*/
    @When("^Hacer click en editar$")
    public void hacer_click_en_editar() throws Throwable
    {
        audit.onClickEdit();
    }

    @When("^Modificar datos de la Auditoria$")
    public void modificar_datos_de_auditoria(DataTable datos) throws Throwable
    {
        audit.modifiedDatos(datos);
    }

    @Then("^Verificar correcta modificacion \"([^\"]*)\"$")
    public void verificar_registro_Modificado(String record) throws Throwable
    {
        audit.verifyRecord(record);
    }

    /*Escenario Eliminar*/
    @When("^Hacer click en la auditoria a eliminar$")
    public void hacer_click_en_el_registro_a_eliminar() throws Throwable
    {
        audit.onClickDelete();
    }

    @When("^Confirmar haciendo click en el boton Delete$")
    public void hacer_click_en_delete() throws Throwable
    {
        audit.onButtonDelete();
    }

}