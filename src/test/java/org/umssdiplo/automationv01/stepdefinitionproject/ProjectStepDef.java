//--------Jimena Arnez Jimenez -------
package org.umssdiplo.automationv01.stepdefinitionproject;

//import browserManager.BrowserManager;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.umssdiplo.automationv01.core.managepage.Project.Project;
import org.umssdiplo.automationv01.core.utils.LoadPage;

public class ProjectStepDef
{
    private Project project;

    @Given("^Acceder a la pagina acme$")
    public void projectPageIsLoaded() throws Throwable
    {
        project = LoadPage.projectPage();
    }

    @When("^Hacer click en la opcion \"([^\"]*)\"$")
    public void navego_al_tab_project(String tabNavegar) throws Throwable
    {
        project.setProject(tabNavegar);
    }

    @When("^Hacer click en el submenu projects \"([^\"]*)\"$")
    public void ingreso_al_tab_projects(String projects) throws Throwable
    {
        project.setProjects(projects);
    }

    @When("^Hacer click en boton para anadir project$")
    public void hacer_click_en_add() throws Throwable
    {
        project.AddNewProject();
    }

    @When("^Llenar datos en el project form$")
    public void lleno_datos_del_Project_form(DataTable datos) throws Throwable
    {
        project.InsertDatos(datos);
    }

    @When("^Hacer click en save project$")
    public void hacer_click_en_save() throws Throwable
    {
        project.onClickSave();
    }


    @When("^Filtrar por nombre de projecto \"([^\"]*)\"$")
    public void filtrar_por_nombre_project(String valor) throws Throwable
    {
        project.filtrarTextBox(valor);
    }


    @Then("^Verificar el projecto creado \"([^\"]*)\"$")
    public void verificar_que_fue_insertado(String record) throws Throwable
    {
        project.verifyRecord(record);
    }

}