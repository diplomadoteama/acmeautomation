//--------Marvin Dickson Mendia Calizaya-------
package org.umssdiplo.automationv01.stepdefinitionproject;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.umssdiplo.automationv01.core.managepage.Role.Role;
import org.umssdiplo.automationv01.core.utils.LoadPage;

public class RoleStepDef
{
    private Role role;

    @Given("^Accedo a la pagina role$")
    public void areaPageIsLoaded() throws Throwable
    {
        role = LoadPage.rolePage();
    }

    @When("^Navego al tab role \"([^\"]*)\"$")
    public void navego_al_tab(String tabNavegar) throws Throwable
    {
        role.setEmployee(tabNavegar);
    }

    @When("^Ingreso al tab para \"([^\"]*)\"$")
    public void ingreso_al_tab_para(String roleT) throws Throwable
    {
        role.setRole(roleT);
    }

    @When("^Hacer click en add role$")
    public void hacer_click_en_add_role() throws Throwable
    {
        role.addNewRole();
    }

    @When("^Lleno datos del Role form$")
    public void lleno_datos_del_Role_form(DataTable datos) throws Throwable
    {
        role.InsertDatos(datos);
    }

    @When("^Hacer click en save de Role$")
    public void hacer_click_en_save_de_Role() throws Throwable
    {
        role.onClickSave();
    }

    @When("^Filtrar por descripcion del Role \"([^\"]*)\"$")
    public void filtrar_por_descripcion_del_Role(String valor) throws Throwable
    {
        role.filtrarTextBox(valor);
    }

    @Then("^Verificar registro insertado en Role \"([^\"]*)\"$")
    public void verificar_registro_insertado_en_Role(String record) throws Throwable
    {
        role.verifyRecord(record);
    }

    /*Escenario Modificar Role*/
    @When("^Hacer click en registro a editar de Role$")
    public void hacer_click_en_registro_a_editar_de_Role() throws Throwable
    {
        role.onClickEdit();
    }

    @When("^Modificar datos del registro de Role$")
    public void modificar_datos_del_registro_de_Role(DataTable datos) throws Throwable
    {
        role.modifiedDatos(datos);
    }

    @Then("^Verificar registro modificado en Role \"([^\"]*)\"$")
    public void verificar_registro_modificado_en_Role(String record) throws Throwable
    {
        role.verifyRecord(record);
    }

    /*Escenario Eliminar Role*/
    @When("^Hacer click en el registro a eliminar de Roles$")
    public void hacer_click_en_el_registro_a_eliminar_de_Roles() throws Throwable
    {
        role.onClickDelete();
    }

    @When("^Hacer click en Delete de Roles$")
    public void hacer_click_en_Delete_de_Roles() throws Throwable
    {
        role.onButtonDelete();
    }

    @When("^Refrescar ventana$")
    public void refrescar_ventana() throws Throwable
    {
        role.onButtonRefresh();
    }
}