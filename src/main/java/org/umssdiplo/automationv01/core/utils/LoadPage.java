package org.umssdiplo.automationv01.core.utils;

import org.umssdiplo.automationv01.core.customwebdriver.ManageDriver;
import org.umssdiplo.automationv01.core.managepage.Area.Area;
import org.umssdiplo.automationv01.core.managepage.Audit.Audit;
import org.umssdiplo.automationv01.core.managepage.Category.Category;
import org.umssdiplo.automationv01.core.managepage.Contract.Contract;
import org.umssdiplo.automationv01.core.managepage.Employee.Employee;
import org.umssdiplo.automationv01.core.managepage.Eventuality.Eventuality;
import org.umssdiplo.automationv01.core.managepage.Home.Header;
import org.umssdiplo.automationv01.core.managepage.Login.Login;
import org.umssdiplo.automationv01.core.managepage.Report.Report;
import org.umssdiplo.automationv01.core.managepage.Role.Role;
import org.umssdiplo.automationv01.core.managepage.Project.Project;

public final class LoadPage
{
    public static Login loginPage()
    {
        ManageDriver.getInstance().getWebDriver()
                .navigate().to(PropertyAccessor.getInstance().getBaseUrl());
        return new Login();
    }

    public static Area areaPage()
    {
        ManageDriver.getInstance().getWebDriver()
                .navigate().to(PropertyAccessor.getInstance().getBaseUrl());
        return new Area();
    }

    public static Role rolePage()
    {
        ManageDriver.getInstance().getWebDriver()
                .navigate().to(PropertyAccessor.getInstance().getBaseUrl());
        return new Role();
    }

    public static Employee employeePage()
    {
        ManageDriver.getInstance().getWebDriver()
                .navigate().to(PropertyAccessor.getInstance().getBaseUrl());
        return new Employee();
    }

    public static Audit auditPage()
    {
        ManageDriver.getInstance().getWebDriver()
                .navigate().to(PropertyAccessor.getInstance().getBaseUrl());
        return new Audit();
    }

    public static Category CategoryPage() {
        ManageDriver.getInstance().getWebDriver()
                .navigate().to(PropertyAccessor.getInstance().getBaseUrl());
        return new Category();
    }
    public static Project projectPage()
    {
        ManageDriver.getInstance().getWebDriver()
                .navigate().to(PropertyAccessor.getInstance().getBaseUrl());
        return new Project();
    }

    public static Report reportPage()
    {
        ManageDriver.getInstance().getWebDriver()
                .navigate().to(PropertyAccessor.getInstance().getBaseUrl());
        return new Report();
    }

    public static Eventuality eventualityPage()
    {
        ManageDriver.getInstance().getWebDriver()
                .navigate().to(PropertyAccessor.getInstance().getBaseUrl());
        return new Eventuality();
    }

    public static Contract contractPage()
    {
        ManageDriver.getInstance().getWebDriver()
                .navigate().to(PropertyAccessor.getInstance().getBaseUrl());
        return new Contract();
    }
    public static Header headerPage()
    {
        ManageDriver.getInstance().getWebDriver()
                .navigate().to(PropertyAccessor.getInstance().getBaseUrl());
        return new Header();
    }
}
