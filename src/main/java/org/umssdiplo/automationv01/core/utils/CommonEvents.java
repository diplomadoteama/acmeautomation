package org.umssdiplo.automationv01.core.utils;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.umssdiplo.automationv01.core.customwebdriver.ManageDriver;

import java.util.List;

public class CommonEvents
{

    /**
     * This method set text content to web element.
     *
     * @param webElement Is web element.
     * @param content    Is the content that will be set to the web element.
     */
    public static void setInputField(WebElement webElement, String content)
    {
        ManageDriver.getInstance().getWebDriverWait().until(ExpectedConditions.visibilityOf(webElement));
        webElement.clear();
        webElement.sendKeys(content);
    }

    /**
     * This method perform a click action in a web element.
     *
     * @param webElement Is the web element that will be pressed.
     */
    public static void clickButton(WebElement webElement)
    {
        ManageDriver.getInstance().getWebDriverWait().until(ExpectedConditions.visibilityOf(webElement));
        webElement.click();
    }

    /**
     * This method perform a click in a non visible element in the UI.
     *
     * @param webElement the WebElement non visible in the UI.
     */
    public static void jsClickElement(WebElement webElement)
    {
        ((JavascriptExecutor) ManageDriver.getInstance().getWebDriver())
                .executeScript("arguments[0].click();", webElement);
    }

    /**
     * This method verifies if a web element is visible.
     *
     * @param webElement is the web element.
     * @return true if web element is visible or false if it isn't visible.
     */
    public static boolean isVisible(WebElement webElement)
    {
        try
        {
            return webElement.isDisplayed();
        } catch (NoSuchElementException e)
        {
            System.out.println("Element do not exits.");
            return false;
        }
    }

    /**
     * This method verifies if a web element is visible.
     *
     * @param webElement is the web element.
     * @return true if web element is visible or false if it isn't visible.
     */
    public static boolean isPresent(WebElement webElement)
    {
        try
        {
            return webElement.isEnabled();
        } catch (NoSuchElementException e)
        {
            System.out.println("Element do not exits.");
            return false;
        }
    }

    /**
     * This method perform a search in a WebElement list based on a content string parameter.
     *
     * @param elements is the WebElements lists.
     * @param content  is the content parameter.
     * @return the WebElement search result.
     */
    public static WebElement findWebElement(List<WebElement> elements, String content)
    {
        return elements.stream()
                .filter(element -> content.equals(element.getText()))
                .findAny()
                .orElse(null);
    }

    /**
     * This method return the text content of a WebElement.
     *
     * @param webElement is the WebElement to extract the text.
     * @return the text content of the WebElement.
     */
    public static String getTextContent(WebElement webElement)
    {
        ManageDriver.getInstance().getWebDriverWait().until(ExpectedConditions.visibilityOf(webElement));
        return webElement.getText();
    }

    public static void waitForEelmentIsNotVisible(WebElement element)
    {
        WebDriverWait wait = new WebDriverWait(ManageDriver.getInstance().getWebDriver(), 10);
        wait.until(ExpectedConditions.invisibilityOf(element));
    }

    /**
     * This method get title of current page.
     *
     * @return title of the current page.
     */
    public static String getPageTitle()
    {
        return ManageDriver.getInstance().getWebDriver().getTitle();
    }

    public static void pressKey(Keys key)
    {
        Actions action = new Actions(ManageDriver.getInstance().getWebDriver());
        action.sendKeys(key).build().perform();
    }

    /**
     * This method press enter key to web element.
     *
     * @param webElement is the WebElement.
     */
    public static void pressEnterKey(WebElement webElement)
    {
        webElement.sendKeys(Keys.ENTER);
    }

    /**
     * Este metodo realiza el evento click a un determinado elemento
     * de una lista dado una cadena de comparacion.
     *
     * @param: lista de WebElement .
     * @param: cadena para el criterio de busqueda.
     */
    public static void elementListClick(List<WebElement> lisWebElements, String elementList){
        List<WebElement> listElement = lisWebElements;
        if (listaVaciaONull(listElement)){
            int i = 0;
            boolean encontrado = false;
            while(i < listElement.size() && encontrado == false){
                if (elementList.equalsIgnoreCase(listElement.get(i).getText())){
                    encontrado = true;
                    CommonEvents.clickButton(listElement.get(i));
                }
                i++;
            }
        }
    }

    private static boolean listaVaciaONull(List<WebElement> lista){
        return null != lista && lista.size() > 0;
    }


    private static String parteFecha(String fecha,String opcion){
        String resultado = "";
        if ( opcion.equalsIgnoreCase("mes")){
            resultado = fecha.substring(0,1);
        }else if(opcion.equalsIgnoreCase("dia")){
            resultado = fecha.substring(3,4);
        }else{
            resultado = fecha.substring(7, fecha.length()-1);
        }
        return resultado;
    }

    /**
     *
     */
    public static boolean verificarMes(WebElement mesAnio, String fecha){
        boolean res = false;
        if (mesAnio.getText().length() > 2){
            String mes = mesAnio.getText().substring(2);
            String mesFecha = parteFecha(fecha,"mes");

            switch (mes){
                case "ENE": res = mesFecha.equalsIgnoreCase("01")? true:false ; break;
                case "FEB": res = mesFecha.equalsIgnoreCase("02")? true:false ; break;
                case "MAR": res = mesFecha.equalsIgnoreCase("03")? true:false ; break;
                case "ABR": res = mesFecha.equalsIgnoreCase("04")? true:false ; break;
                case "MAY": res = mesFecha.equalsIgnoreCase("05")? true:false ; break;
                case "JUN": res = mesFecha.equalsIgnoreCase("06")? true:false ; break;
                case "JUL": res = mesFecha.equalsIgnoreCase("07")? true:false ; break;
                case "AGO": res = mesFecha.equalsIgnoreCase("08")? true:false ; break;
                case "SEP": res = mesFecha.equalsIgnoreCase("09")? true:false ; break;
                case "OCT": res = mesFecha.equalsIgnoreCase("10")? true:false ; break;
                case "NOV": res = mesFecha.equalsIgnoreCase("11")? true:false ; break;
                case "DIC": res = mesFecha.equalsIgnoreCase("12")? true:false ; break;
                default: res= false;
            }
        }

        return res;

    }

    public static boolean verificarAnio(WebElement mesAnio, String fecha){
        boolean resAnio = false;
        String dato = mesAnio.getText();
        String anio = dato.substring(4, dato.length()-1);
        String anioDato = parteFecha(fecha,"anio");
        return anio.equalsIgnoreCase(anioDato);
    }


    public static void seccionaDiaClick(List<WebElement> dias, String fecha){
        boolean res = false;
        if (dias.size() > 0) {
            int i = 0;
            String diaFecha = parteFecha(fecha, "dia");
            boolean encontrado = false;
            while (i < 32 && encontrado == false) {
                if (dias.get(i).getText().equalsIgnoreCase(diaFecha)) {
                    encontrado = true;
                    dias.get(i).click();
                }
            }
        }
    }

}
