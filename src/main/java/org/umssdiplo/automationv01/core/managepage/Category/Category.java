package org.umssdiplo.automationv01.core.managepage.Category;

import cucumber.api.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.umssdiplo.automationv01.core.customwebdriver.ManageDriver;
import org.umssdiplo.automationv01.core.managepage.BasePage;
import org.umssdiplo.automationv01.core.utils.CommonEvents;

import java.util.List;

public class Category extends BasePage {
    @FindBy(xpath = "//*[@id=\"menuEmployee\"]/span")
    private WebElement tabEmployee;

    @FindBy(xpath = "//a[contains(text(),'Category')]")
    private WebElement tabItems;

    @FindBy(id = "addcategory")
    private WebElement buttonAddNewCategory;

    @FindBy(xpath = "//textarea[@name='code']")
    private WebElement codeTextBox;

    @FindBy(xpath = "//textarea[@name='Name']")
    private WebElement nameTextBox;

    @FindBy(xpath = "//textarea[@name='Description']")
    private WebElement descriptionTextBox;

    @FindBy(xpath = "//span[contains(text(),'Save')]")
    private WebElement buttonCategorySave;

    @FindBy(xpath = "//button[@id='delete_category0']/span/mat-icon")
    private WebElement deleteCategorybutton;

    @FindBy(id = "deletebutton")
    private WebElement buttonDelete;

    @FindBy(xpath = "//button[@id='edit_category0']/span/mat-icon")
    private WebElement editCategorybutton;



    @FindBy(id = "editCode" )
    private WebElement editCode;

    @FindBy(id = "editName")
    private WebElement editName;

    @FindBy(id = "editDescription")
    private WebElement editDescription;

    @FindBy(id = "saveupdate")
    private WebElement saveUpdate;


    public void setCategory() {
        CommonEvents.clickButton(tabEmployee);
    }

    public void setTabCategories() {
        WebElement tabItems = (new WebDriverWait(ManageDriver.getInstance().getWebDriver(), 60)).until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'Employee')]")));
        CommonEvents.pressEnterKey(tabEmployee.findElement(By.xpath("//button[contains(text(),'Category')]")));
    }

    public void AddNewCategory() {
        if (buttonAddNewCategory != null) {
            CommonEvents.clickButton(buttonAddNewCategory);
        }
    }

    public void InsertCategory(DataTable datos) {
        List<List<String>> datosform = datos.raw();
        for (List<String> dato : datosform)
            switch (dato.get(0)) {
                case "code":
                    CommonEvents.setInputField(codeTextBox, dato.get(1));
                    break;
                case "Name":
                    CommonEvents.setInputField(nameTextBox, dato.get(1));
                    break;
                case "Description":
                    CommonEvents.setInputField(descriptionTextBox, dato.get(1));
                    break;
            }
    }

    public void sendForm() {
        CommonEvents.clickButton(buttonCategorySave);
    }

    public void deleteCategory() {
        CommonEvents.clickButton(deleteCategorybutton);
    }

    public void deleteCategoryButton() {
        if (buttonDelete != null) {
            CommonEvents.clickButton(buttonDelete);
        }
    }

    public void categoryDialogEdit() {
        CommonEvents.clickButton(editCategorybutton);
    }

    public void saveDialogCategoryButton() {
        if (buttonDelete != null) {
            CommonEvents.clickButton(buttonDelete);
        }
    }

    public void cambiarDatosForm(DataTable datos) {
        List<List<String>> datosCategory = datos.raw();
        for (List<String> dato : datosCategory)
            switch (dato.get(0)) {
                case "code":
                    editCode.clear();
                    CommonEvents.setInputField(editCode, dato.get(1));
                    break;
                case "Name":
                    editName.clear();
                    CommonEvents.setInputField(editName, dato.get(1));
                    break;
                case "Description":
                    editDescription.clear();
                    CommonEvents.setInputField(editDescription, dato.get(1));
                    break;
            }
    }

    public void saveUpdate()
    {
        CommonEvents.clickButton(saveUpdate);
    }

    public void closeWindow() {
        waitForNextAction();
        webDriver.close();
    }

    public void waitForNextAction() {
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void verifyRecord(String record) {

    }
}


