//--------ACC-------
package org.umssdiplo.automationv01.core.managepage.Audit;

import cucumber.api.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.umssdiplo.automationv01.core.customwebdriver.ManageDriver;
import org.umssdiplo.automationv01.core.managepage.BasePage;
import org.umssdiplo.automationv01.core.utils.CommonEvents;

import java.util.List;

//import org.testng.Assert;

public class Audit extends BasePage
{
    @FindBy(xpath = "//span[contains(text(),'Audits')]")
    private WebElement tabAudits1;

    @FindBy(xpath = "//button[contains(text(),'Audit')]")
    private WebElement tabAudit1;

    @FindBy(xpath = "//button[@class='mat-icon-button mat-primary']")
    private WebElement buttonAddNewAudit;

    @FindBy(xpath = "//textarea[@name='auditName']")
    private WebElement nameTextBox;

    @FindBy(xpath = "//textarea[@name='auditCode']")
    private WebElement codeTextBox;

    @FindBy(xpath = "//textarea[@name='auditType']")
    private WebElement typeTextBox;

    @FindBy(xpath = "//textarea[@name='auditPeriodicity']")
    private WebElement periodicityTextBox;

    @FindBy(xpath = "//textarea[@name='auditScope']")
    private WebElement scopeTextBox;

    @FindBy(xpath = "//textarea[@name='auditObjective']")
    private WebElement objectiveTextBox;

    @FindBy(xpath = "//textarea[@name='auditCriteria']")
    private WebElement criteriaTextBox;



    @FindBy(xpath = "//span[contains(text(),'Save')]")
    private WebElement buttonSave;

    @FindBy(xpath = "//div[@class='mat-select-arrow-wrapper']")
    private WebElement buttonPag;

    @FindBy(xpath = "//input[@class='mat-input-element mat-form-field-autofill-control']")
    private WebElement filtrarText;

    @FindBy(xpath = "//button[@id='edit_audit0']/span/mat-icon")
    private WebElement iconEditar;

    @FindBy(xpath = "//button[@id='delete_audit0']/span/mat-icon")
    private WebElement iconDelete;

    @FindBy(xpath = "//span[contains(text(),'Delete')]")
    private WebElement buttonDelete;

    @FindBy(xpath = "//div[contains(text(),'No results')]")
    private WebElement noResult;

    @FindBy(xpath = "//mat-cell[contains(text(),'Quality')]")
    private WebElement newRecord;

    @FindBy(xpath = "//mat-cell[contains(text(),'Modified')]")
    private WebElement modifyRecord;

    public void setAudits(String tabAudits)
    {
        CommonEvents.clickButton(tabAudits1);
    }

    public void setAudit(String tabAudit)
    {
        WebElement tabAudit2 = (new WebDriverWait(ManageDriver.getInstance().getWebDriver(), 30)).until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'Audit')]")));
        //tabArea.findElement(By.xpath("//button[contains(text(),'Areas')]")).click();
        CommonEvents.pressEnterKey(tabAudit2.findElement(By.xpath("//button[contains(text(),'Audit')]")));
    }

    public void AddNewAudit()
    {
        CommonEvents.clickButton(buttonAddNewAudit);
    }

    public void InsertDatos(DataTable datos)
    {
        List<List<String>> datosAudit = datos.raw();
        for (List<String> dato : datosAudit)
            switch (dato.get(0))
            {
                case "Audit Name":
                    //codeTextBox().clear();
                    CommonEvents.setInputField(nameTextBox, dato.get(1));
                    break;
                case "Audit Code":
                    //nameTextBox().clear();
                    CommonEvents.setInputField(codeTextBox, dato.get(1));
                    break;
                case "Audit Scope":
                    //nameTextBox().clear();
                    CommonEvents.setInputField(scopeTextBox, dato.get(1));
                    break;
                case "Audit Objective":
                    //nameTextBox().clear();
                    CommonEvents.setInputField(objectiveTextBox, dato.get(1));
                    break;
                case "Audit Criteria":
                    //nameTextBox().clear();
                    CommonEvents.setInputField(criteriaTextBox, dato.get(1));
                    break;
            }
    }

    public void verifyRecord(String record)
    {
        /*WebElement expectedRecord = ManageDriver.getInstance().getWebDriver()
                .findElement(By.xpath("//mat-cell[contains(text(),'" + record + "')]"));
        Assert.assertTrue(CommonEvents.isVisible(expectedRecord));*/
        CommonEvents.isPresent(newRecord);
    }

    public void onClickSave()
    {
        CommonEvents.clickButton(buttonSave);
    }

    public void filtrarTextBox(String valor)
    {
        CommonEvents.setInputField(filtrarText, valor);
    }

    public void onClickEdit()
    {
        CommonEvents.clickButton(iconEditar);
    }

    public void modifiedDatos(DataTable datos)
    {
        List<List<String>> datosAudit = datos.raw();
        for (List<String> dato : datosAudit)
            switch (dato.get(0))
            {
                case "Audit Name":
                    //codeTextBox().clear();
                    CommonEvents.setInputField(nameTextBox, dato.get(1));
                    break;
                case "Audit Code":
                    //nameTextBox().clear();
                    CommonEvents.setInputField(codeTextBox, dato.get(1));
                    break;
                case "Audit Type":
                    //nameTextBox().clear();
                    CommonEvents.setInputField(typeTextBox, dato.get(1));
                    break;
                case "Audit Periodicity":
                    //nameTextBox().clear();
                    CommonEvents.setInputField(periodicityTextBox, dato.get(1));
                    break;
            }
    }

    public void onClickDelete()
    {
        CommonEvents.clickButton(iconDelete);
    }

    public void onButtonDelete()
    {
        CommonEvents.clickButton(buttonDelete);
    }


    public void NoResults(String result)
    {
        CommonEvents.isPresent(noResult);
    }
}