//--------Marvin Dickson Mendia Calizaya-------
package org.umssdiplo.automationv01.core.managepage.Home;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.umssdiplo.automationv01.core.managepage.BasePage;

public class Header extends BasePage
{
    @FindBy(xpath = "//a[contains(text(),'All Courses')]")
    private WebElement allCoursesTab;

    public void goToAllCoursesTab()
    {
        allCoursesTab.click();
    }


    @FindBy(css = "button.mat-button:nth-child(5)")
    private WebElement contractTb;

    @FindBy(css = "button.mat-menu-item:nth-child(2)")
    private WebElement registerContractOp;

    public WebElement contractTbClick() {
        return contractTb;
    }

    public WebElement registerContractOpClick() {
        return registerContractOp;
    }

    @Override
    public void verifyRecord(String record) {

    }
}