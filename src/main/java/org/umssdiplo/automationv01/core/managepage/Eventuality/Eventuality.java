
package org.umssdiplo.automationv01.core.managepage.Eventuality;

import cucumber.api.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.umssdiplo.automationv01.core.customwebdriver.ManageDriver;
import org.umssdiplo.automationv01.core.managepage.BasePage;
import org.umssdiplo.automationv01.core.utils.CommonEvents;

import java.util.List;

public class Eventuality extends BasePage
{
    @FindBy(xpath = "//span[contains(text(),'Incident and Accident')]")
    private WebElement tabEventuality;

    @FindBy(xpath = "//button[contains(text(),'Incident and Accident')]")
    private WebElement tabEventualityOption;

    @FindBy(xpath = "/html/body/app-root/main/app-eventuality/div[2]/mat-table/mat-header-row/mat-header-cell[8]/button/span/mat-icon")
    private WebElement buttonAddNewEventuality;

    @FindBy(xpath = "//span[contains(text(),'Type Event')]")
    private WebElement dropdownTypeEvent;

    @FindBy(xpath = "//span[contains(text(),'Accidente')]")
    private WebElement dropdownAccidente;

    @FindBy(name = "dateEvent")
    private WebElement dateEvenField;

    @FindBy(xpath = "//span[contains(text(),'Injury Type')]")
    private WebElement injuryTypeDropdown;

    @FindBy(xpath = "//span[contains(text(),'Escoriaciones')]")
    private WebElement escoracionesOption;

    @FindBy(xpath = "//span[contains(text(),'Injury Part')]")
    private WebElement injuryPartDropdown;

    @FindBy(xpath = "//span[contains(text(),'Region craneana')]")
    private WebElement regionCreananaOption;

    @FindBy(name = "description")
    private WebElement descriptionEvent;

    @FindBy(xpath = "//span[contains(text(),'Employee ID')]")
    private WebElement employeeDropdown;

    @FindBy(xpath = "//span[contains(text(),'Miguel Lopez')]")
    private WebElement MiguelLopezOption;

    @FindBy(xpath = "//span[contains(text(),'Save')]")
    private WebElement buttonSave;

    @FindBy(xpath = "//div[@class='mat-select-arrow-wrapper']")
    private WebElement buttonPag;

    @FindBy(xpath = "//input[@class='mat-input-element mat-form-field-autofill-control']")
    private WebElement filtrarText;

    @FindBy(xpath = "//button[@id='edit_audit0']/span/mat-icon")
    private WebElement iconEditar;

    @FindBy(xpath = "//button[@id='delete_audit0']/span/mat-icon")
    private WebElement iconDelete;

    @FindBy(xpath = "//span[contains(text(),'Delete')]")
    private WebElement buttonDelete;

    @FindBy(xpath = "//div[contains(text(),'No results')]")
    private WebElement noResult;

    @FindBy(xpath = "//mat-cell[contains(text(),'Quality')]")
    private WebElement newRecord;

    @FindBy(xpath = "//mat-cell[contains(text(),'Modified')]")
    private WebElement modifyRecord;

    public void setEventualities()
    {
        CommonEvents.clickButton(tabEventuality);
    }

    public void setEventuality()
    {
        WebElement tabEventuality2 = (new WebDriverWait(ManageDriver.getInstance().getWebDriver(), 30)).until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'Incident and Accident')]")));
        CommonEvents.pressEnterKey(tabEventuality2.findElement(By.xpath("//button[contains(text(),'Incident and Accident')]")));
    }

    public void AddNewEventuality()
    {
        CommonEvents.clickButton(buttonAddNewEventuality);
    }

    public void SelectAccident(){
        CommonEvents.jsClickElement(dropdownTypeEvent);
        CommonEvents.jsClickElement(dropdownAccidente);
    }

    public void FillOutData(DataTable datos)
    {
        List<List<String>> datosEventuality = datos.raw();
        for (List<String> dato : datosEventuality)
            switch (dato.get(0))
            {
                case "Date of event":
                    CommonEvents.setInputField(dateEvenField, dato.get(1));
                    break;
                case "Injury Type":
                    CommonEvents.jsClickElement(injuryTypeDropdown);
                    CommonEvents.jsClickElement(escoracionesOption);
                    break;
                case "Injury Part":
                    CommonEvents.jsClickElement(injuryPartDropdown);
                    CommonEvents.jsClickElement(regionCreananaOption);
                    break;
                case "Leave a description":
                    //nameTextBox().clear();
                    CommonEvents.setInputField(descriptionEvent, dato.get(1));
                    break;
                case "Employee ID":
                    CommonEvents.jsClickElement(employeeDropdown);
                    CommonEvents.jsClickElement(MiguelLopezOption);
                    break;
            }
    }

    public void verifyRecord(String record)
    {
        CommonEvents.isPresent(newRecord);
    }

    public void onClickSave()
    {
        CommonEvents.clickButton(buttonSave);
    }

    public void filtrarTextBox(String valor)
    {
        CommonEvents.setInputField(filtrarText, valor);
    }
}