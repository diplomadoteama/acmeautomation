//---------------
package org.umssdiplo.automationv01.core.managepage.Report;

import cucumber.api.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.umssdiplo.automationv01.core.customwebdriver.ManageDriver;
import org.umssdiplo.automationv01.core.managepage.BasePage;
import org.umssdiplo.automationv01.core.utils.CommonEvents;

import java.util.List;


public class Report extends BasePage
{
    @FindBy(xpath = "//span[contains(text(),'Report')]")
    private WebElement tabReport;

    @FindBy(xpath = "//button[contains(text(),'Graphic Report')]")
    private WebElement tabGraphicReport;


    public void setReport(String tabNavegar)
    {
        CommonEvents.clickButton(tabReport);
    }

    public void setGraphicReport(String ssi)
    {
        WebElement tabGraphicReport = (new WebDriverWait(ManageDriver.getInstance().getWebDriver(), 30)).until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'Report')]")));
        //tabArea.findElement(By.xpath("//button[contains(text(),'Areas')]")).click();
        CommonEvents.pressEnterKey(tabGraphicReport.findElement(By.xpath("//button[contains(text(),'Graphic Report')]")));
    }


    @FindBy(id="employeeid")
    private WebElement employeeTextBox;
    public void InsertDatos(DataTable datos)
    {
        List<List<String>> datosReporte = datos.raw();
        for (List<String> dato : datosReporte)
            switch (dato.get(0))
            {
                case "employee":
                    CommonEvents.setInputField(employeeTextBox, dato.get(1));
                    break;
            }
    }


    @FindBy(id="load")
    private WebElement buttonLoad;

    public void onClickLoad()
    {
        CommonEvents.clickButton(buttonLoad);
    }


    @Override
    public void verifyRecord(String record) {

    }
}