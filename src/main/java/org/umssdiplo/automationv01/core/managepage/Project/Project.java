//--------Jimena Arnez Jimenez-------
package org.umssdiplo.automationv01.core.managepage.Project;

import cucumber.api.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.umssdiplo.automationv01.core.customwebdriver.ManageDriver;
import org.umssdiplo.automationv01.core.managepage.BasePage;
import org.umssdiplo.automationv01.core.utils.CommonEvents;

import java.util.List;



public class Project extends BasePage
{
    @FindBy(xpath = "//span[contains(text(),'Project')]")
    private WebElement tabProject;
    public void setProject(String tabNavegar)
    {
        CommonEvents.clickButton(tabProject);
    }


    @FindBy(xpath = "//button[contains(text(),'Projects')]")
    private WebElement tabProjects;
    public void setProjects(String ssi)
    {
        WebElement tabProjects = (new WebDriverWait(ManageDriver.getInstance().getWebDriver(), 30)).until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'Projects')]")));
        CommonEvents.pressEnterKey(tabProjects.findElement(By.xpath("//button[contains(text(),'Projects')]")));
    }

    //@FindBy(id="addNewProject")////mat-icon[contains(text(),'add')]
    @FindBy(xpath = "/html/body/app-root/main/app-project/div/mat-table/mat-header-row/mat-header-cell[7]/button/span/mat-icon")
    private WebElement buttonAddNewProject;

    public void AddNewProject()
    {
        if (buttonAddNewProject != null)
        {
            CommonEvents.clickButton(buttonAddNewProject);
        }
    }


    @FindBy(id="projectname")
    //@FindBy(xpath = "//textarea[@name='projectname']")
    private WebElement projectNameTextBox;

    @FindBy(id="description")
    private WebElement descriptionTextBox;

    @FindBy(id="dateStart")
    //@FindBy(xpath = "//textarea[@name='date_start']")
    private WebElement dateStartPicker;

    @FindBy(id="dateEnd")
    //@FindBy(xpath = "//textarea[@name='date_end']")
    private WebElement dateEndPicker;

    public void InsertDatos(DataTable datos)
    {
        List<List<String>> datosProject = datos.raw();
        for (List<String> dato : datosProject)
            switch (dato.get(0))
            {
                case "projectname":
                    //codeTextBox().clear();
                    CommonEvents.setInputField(projectNameTextBox, dato.get(1));
                    break;
                case "description":
                    //nameTextBox().clear();
                    CommonEvents.setInputField(descriptionTextBox, dato.get(1));
                    break;
                case "date_start":
                    //codeTextBox().clear();
                    CommonEvents.setInputField(dateStartPicker, dato.get(1));
                    break;
                case "date_end":
                    //nameTextBox().clear();
                    CommonEvents.setInputField(dateEndPicker, dato.get(1));
                    break;
            }
    }

    @FindBy(xpath = "//span[contains(text(),'Save')]")
    private WebElement buttonSave;
    public void onClickSave()
    {
        CommonEvents.clickButton(buttonSave);
    }


    @FindBy(xpath = "//input[@class='mat-input-element mat-form-field-autofill-control']")
    private WebElement filtrarText;
    public void filtrarTextBox(String valor)
    {
        CommonEvents.setInputField(filtrarText, valor);
    }

    @FindBy(xpath = "//mat-cell[contains(text(),'Quality')]")
    private WebElement newRecord;
    public void verifyRecord(String record)
    {
        CommonEvents.isPresent(newRecord);
    }

}