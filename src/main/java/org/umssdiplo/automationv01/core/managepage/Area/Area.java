//--------Marvin Dickson Mendia Calizaya-------
package org.umssdiplo.automationv01.core.managepage.Area;

import cucumber.api.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.umssdiplo.automationv01.core.customwebdriver.ManageDriver;
import org.umssdiplo.automationv01.core.managepage.BasePage;
import org.umssdiplo.automationv01.core.utils.CommonEvents;

import java.util.List;

//import org.testng.Assert;

public class Area extends BasePage
{
    @FindBy(xpath = "//span[contains(text(),'Employee')]")
    private WebElement tabEmployee;

    @FindBy(xpath = "//button[contains(text(),'Areas')]")
    private WebElement tabAreas;

    @FindBy(xpath = "//button[@class='mat-icon-button mat-primary']")
    private WebElement buttonAddNewArea;

    @FindBy(xpath = "//textarea[@name='code']")
    private WebElement codeTextBox;

    @FindBy(xpath = "//textarea[@name='name']")
    private WebElement nameTextBox;

    @FindBy(xpath = "//span[contains(text(),'Save')]")
    private WebElement buttonSave;

    @FindBy(xpath = "//div[@class='mat-select-arrow-wrapper']")
    private WebElement buttonPag;

    @FindBy(xpath = "//input[@class='mat-input-element mat-form-field-autofill-control']")
    private WebElement filtrarText;

    @FindBy(xpath = "//button[@id='edit_area0']/span/mat-icon")
    private WebElement iconEditar;

    @FindBy(xpath = "//button[@id='delete_area0']/span/mat-icon")
    private WebElement iconDelete;

    @FindBy(xpath = "//span[contains(text(),'Delete')]")
    private WebElement buttonDelete;

    @FindBy(xpath = "//div[contains(text(),'No results')]")
    private WebElement noResult;

    @FindBy(xpath = "//mat-cell[contains(text(),'Quality')]")
    private WebElement newRecord;

    @FindBy(xpath = "//mat-cell[contains(text(),'Modified')]")
    private WebElement modifyRecord;

    public void setEmployee(String tabNavegar)
    {
        CommonEvents.clickButton(tabEmployee);
    }

    public void setArea(String ssi)
    {
        WebElement tabArea = (new WebDriverWait(ManageDriver.getInstance().getWebDriver(), 30)).until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'Areas')]")));
        //tabArea.findElement(By.xpath("//button[contains(text(),'Areas')]")).click();
        CommonEvents.pressEnterKey(tabArea.findElement(By.xpath("//button[contains(text(),'Areas')]")));
    }

    public void AddNewArea()
    {
        CommonEvents.clickButton(buttonAddNewArea);
    }

    public void InsertDatos(DataTable datos)
    {
        List<List<String>> datosArea = datos.raw();
        for (List<String> dato : datosArea)
            switch (dato.get(0))
            {
                case "code":
                    //codeTextBox().clear();
                    CommonEvents.setInputField(codeTextBox, dato.get(1));
                    break;
                case "name":
                    //nameTextBox().clear();
                    CommonEvents.setInputField(nameTextBox, dato.get(1));
                    break;
            }
    }

    public void verifyRecord(String record)
    {
        /*WebElement expectedRecord = ManageDriver.getInstance().getWebDriver()
                .findElement(By.xpath("//mat-cell[contains(text(),'" + record + "')]"));
        Assert.assertTrue(CommonEvents.isVisible(expectedRecord));*/
        CommonEvents.isPresent(newRecord);
    }

    public void onClickSave()
    {
        CommonEvents.clickButton(buttonSave);
    }

    public void filtrarTextBox(String valor)
    {
        CommonEvents.setInputField(filtrarText, valor);
    }

    public void onClickEdit()
    {
        CommonEvents.clickButton(iconEditar);
    }

    public void modifiedDatos(DataTable datos)
    {
        List<List<String>> datosArea = datos.raw();
        for (List<String> dato : datosArea)
            switch (dato.get(0))
            {
                case "code":
                    codeTextBox.clear();
                    CommonEvents.setInputField(codeTextBox, dato.get(1));
                    break;
                case "name":
                    nameTextBox.clear();
                    CommonEvents.setInputField(nameTextBox, dato.get(1));
                    break;
            }
    }

    public void onClickDelete()
    {
        CommonEvents.clickButton(iconDelete);
    }

    public void onButtonDelete()
    {
        CommonEvents.clickButton(buttonDelete);
    }


    public void NoResult(String result)
    {
        /*WebElement expectedResult = ManageDriver.getInstance().getWebDriver()
                .findElement(By.xpath("//div[contains(text(),'" + result + "')]"));
        Assert.assertTrue(CommonEvents.isVisible(expectedResult));*/
        CommonEvents.isPresent(noResult);
    }
}