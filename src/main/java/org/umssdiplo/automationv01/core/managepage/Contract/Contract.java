package org.umssdiplo.automationv01.core.managepage.Contract;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.umssdiplo.automationv01.core.managepage.BasePage;
import org.umssdiplo.automationv01.core.utils.CommonEvents;

import java.util.List;

/**
 * Description the this class Contract
 *
 * @author: Marcelo Vargas
 * @version: 30/07/2018
 */
public class Contract extends BasePage {

    @FindBy(css = "input[id^='mat-input'][formcontrolname='code']")
    private WebElement codigoTxt;

    @FindBy(css = "mat-select[id^='mat-select'][formcontrolname='typeForm']")
    private List<WebElement> tipoList;

    @FindBy(css = "mat-select[id^='mat-select'][formcontrolname='projectForm']")
    private List<WebElement> proyectoList;

    @FindBy(css = "input[id^='mat-input'][formcontrolname='beginDate']")
    private WebElement fechaInicio;

    @FindBy(css = "input[id^='mat-input'][formcontrolname='beginDate']")
    private WebElement fechaFin;

    @FindBy(css = "mat-select[id^='mat-select'][formcontrolname='workPositionForm']")
    private List<WebElement> posicionList;

    @FindBy(css = "input[id^='mat-input'][formcontrolname='amount']")
    private WebElement montoPago;

    @FindBy(css = "mat-select[id^='mat-select'][formcontrolname='typePaymentForm']")
    private List<WebElement> tipoPagoList;

    @FindBy(css = "input[id^='mat-select'][formcontrolname='employeeForm']")
    private List<WebElement> empleadoList;

    @FindBy(css = "input[id^='mat-select'][formcontrolname='employeeForm']")
    private WebElement empleadoTxt;

    @FindBy(css = ".btn mat-button")
    private WebElement saveBtn;

    @FindBy(css = "mat-calendar[id='mat-datepicker-0'] .mat-calendar-controls .Choose.month.and.year  .mat-calendar-period-button")
    private WebElement calendarBtn;

    @FindBy(css = "mat-calendar[id='mat-datepicker-0'] .mat-calendar-controls button.mat-calendar-period-button.mat-button")
    private WebElement mesAnioBtn;

    @FindBy(css = "mat-calendar[id='mat-datepicker-0'] .mat-calendar-controls button.mat-calendar-previous-button.mat-icon-button")
    private WebElement anioAtrasBtn;

    @FindBy(css = "mat-calendar[id='mat-datepicker-0'] .mat-calendar-controls button.mat-calendar-next-button.mat-icon-button")
    private WebElement siguienteBtn;

    @FindBy(css = ".mat-calendar-table td div")
    private List<WebElement> dia;




    public List<WebElement> getTipoList() {
        return tipoList;
    }

//    public void elementListClick(String elementList){
//        List<WebElement> listElement = this.getTipoList();
//        if (null != listElement && listElement.size() > 0){
//            int i = 0;
//            while(i < listElement.size() && elementList.toLowerCase().equals(listElement.get(i).getText().toLowerCase())){
//                CommonEvents.clickButton(listElement.get(i));
//                i++;
//            }
//        }
//    }

    public void tipoElementListClick(String elementList){
        CommonEvents.elementListClick(this.getTipoList(), elementList);
    }

    public WebElement getCodigoTxt() {
        return codigoTxt;
    }

    public List<WebElement> getProyectoList() {
        return proyectoList;
    }

    public void proyectoElementListClick(String elementList){
        CommonEvents.elementListClick(this.getProyectoList(), elementList);
    }

    public WebElement getFechaInicio() {
        return fechaInicio;
    }

    public WebElement getFechaFin() {
        return fechaFin;
    }

    public List<WebElement> getPosicionList() {
        return posicionList;
    }

    public void posicionElementListClick(String elementList){
        CommonEvents.elementListClick(this.getPosicionList(), elementList);
    }

    public WebElement getMontoPago() {
        return montoPago;
    }

    public List<WebElement> getTipoPagoList() {
        return tipoPagoList;
    }

    public void tipoPagoElementListClick(String elementList){
        CommonEvents.elementListClick(this.getTipoPagoList(), elementList);
    }

    public List<WebElement> getEmpleadoList() {
        return empleadoList;
    }

    public void empleadoElementListClick(String elementList){
        CommonEvents.elementListClick(this.getEmpleadoList(), elementList);
    }

    public void setEmpleado(String empleado){
        this.empleadoTxt.sendKeys(empleado);
    }

    public WebElement getSaveBtn() {
        return saveBtn;
    }

    public void setSaveBtn(WebElement saveBtn) {
        this.saveBtn = saveBtn;
    }

    public void setCodigoTxt(String codigoTxt) {
        this.codigoTxt.sendKeys(codigoTxt);
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio.sendKeys(fechaInicio);
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin.sendKeys(fechaFin);
    }

    public void setMontoPago(String montoPago) {
        this.montoPago.sendKeys(montoPago);
    }

    public void saveBtnClick() {
        CommonEvents.clickButton(this.saveBtn);
    }

    @Override
    public void verifyRecord(String record) {

    }
}