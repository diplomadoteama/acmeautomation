//--------Marvin Dickson Mendia Calizaya-------
package org.umssdiplo.automationv01.core.managepage.Role;

import cucumber.api.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.umssdiplo.automationv01.core.customwebdriver.ManageDriver;
import org.umssdiplo.automationv01.core.managepage.BasePage;
import org.umssdiplo.automationv01.core.utils.CommonEvents;

import java.util.List;

public class Role extends BasePage
{
    @FindBy(xpath = "//button[@id='menuEmployee']")
    private WebElement tabEmployee;

    @FindBy(xpath = "//button[contains(text(),'Roles')]")
    private WebElement tabRoles;

    @FindBy(xpath = "//button[@class='mat-icon-button mat-primary']")
    private WebElement buttonAddNewRole;

    @FindBy(xpath = "//textarea[@name='code']")
    private WebElement codeTextBox;

    @FindBy(xpath = "//textarea[@name='description']")
    private WebElement descriptionTextBox;

    @FindBy(xpath = "//span[contains(text(),'Save')]")
    private WebElement buttonSave;

    @FindBy(xpath = "//input[@class='mat-input-element mat-form-field-autofill-control']")
    private WebElement filtrarText;

    @FindBy(xpath = "//mat-cell[contains(text(),'ExampleRole')]")
    private WebElement newRecord;

    @FindBy(xpath = "//button[@id='edit_role0']/span/mat-icon")
    private WebElement iconEditar;

    @FindBy(xpath = "//button[@id='delete_role0']/span/mat-icon")
    private WebElement iconDelete;

    @FindBy(xpath = "//span[contains(text(),'Delete')]")
    private WebElement buttonDelete;

    @FindBy(xpath = "//mat-icon[contains(text(),'refresh')]")
    private WebElement buttonRefresh;

    public void setEmployee(String tabNavegar)
    {
        CommonEvents.pressEnterKey(tabEmployee);
    }

    public void setRole(String ssi)
    {
        WebElement tabRole = (new WebDriverWait(ManageDriver.getInstance().getWebDriver(), 30)).until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'Roles')]")));
        CommonEvents.pressEnterKey(tabRole.findElement(By.xpath("//button[contains(text(),'Roles')]")));
    }

    public void addNewRole()
    {
        CommonEvents.clickButton(buttonAddNewRole);
    }

    public void InsertDatos(DataTable datos)
    {
        List<List<String>> datosRole = datos.raw();
        for (List<String> dato : datosRole)
            switch (dato.get(0))
            {
                case "code":
                    CommonEvents.setInputField(codeTextBox, dato.get(1));
                    break;
                case "description":
                    CommonEvents.setInputField(descriptionTextBox, dato.get(1));
                    break;
            }
    }

    public void onClickSave()
    {
        CommonEvents.clickButton(buttonSave);
    }

    public void filtrarTextBox(String valor)
    {
        CommonEvents.setInputField(filtrarText, valor);
    }

    public void verifyRecord(String record)
    {
        /*WebElement expectedRecord = ManageDriver.getInstance().getWebDriver()
                .findElement(By.xpath("//mat-cell[contains(text(),'" + record + "')]"));
        Assert.assertTrue(CommonEvents.isVisible(expectedRecord));*/
        CommonEvents.isPresent(newRecord);
    }

    public void onClickEdit()
    {
        CommonEvents.clickButton(iconEditar);
    }

    public void modifiedDatos(DataTable datos)
    {
        List<List<String>> datosRole = datos.raw();
        for (List<String> dato : datosRole)
            switch (dato.get(0))
            {
                case "code":
                    codeTextBox.clear();
                    CommonEvents.setInputField(codeTextBox, dato.get(1));
                    break;
                case "description":
                    descriptionTextBox.clear();
                    CommonEvents.setInputField(descriptionTextBox, dato.get(1));
                    break;
            }
    }

    public void onClickDelete()
    {
        CommonEvents.clickButton(iconDelete);
    }

    public void onButtonDelete()
    {
        CommonEvents.clickButton(buttonDelete);
    }

    public void onButtonRefresh()
    {
        CommonEvents.clickButton(buttonRefresh);
    }
}