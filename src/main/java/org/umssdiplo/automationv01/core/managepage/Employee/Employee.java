package org.umssdiplo.automationv01.core.managepage.Employee;

import cucumber.api.DataTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.umssdiplo.automationv01.core.customwebdriver.ManageDriver;
import org.umssdiplo.automationv01.core.managepage.BasePage;
import org.umssdiplo.automationv01.core.utils.CommonEvents;

import java.util.List;

public class Employee extends BasePage
{
    @FindBy(xpath = "//*[@id=\"menuEmployee\"]/span")
    private WebElement tabEmployee;

    @FindBy(xpath = "//a[contains(text(),'Employees')]")
    private WebElement tabEmployees;

    @FindBy(id = "addNewEmployee")
    private WebElement buttonAddNewEmployee;

    @FindBy(xpath = "//textarea[@name='firstName']")
    private WebElement firstNameTextBox;

    @FindBy(xpath = "//textarea[@name='lastName']")
    private WebElement lastNameTextBox;

    @FindBy(xpath = "//textarea[@name='address']")
    private WebElement addressTextBox;

    @FindBy(xpath = "//input[@name='phone']")
    private WebElement phoneTextBox;

    @FindBy(xpath = "//textarea[@name='email']")
    private WebElement emailTextBox;

    @FindBy(xpath = "//textarea[@name='dni']")
    private WebElement dniTextBox;

    @FindBy(xpath = "//input[@name='job Position']")
    private WebElement jobPositionTextBox;

    @FindBy(xpath = "//span[contains(text(),'Save')]")
    private WebElement buttonSave;

    @FindBy(xpath = "//button//mat-icon[@aria-label='Delete']")
    private WebElement deleteIcon;

    @FindBy(xpath = "//span[contains(text(),'Delete')]")
    private WebElement deleteButton;

    @FindBy(xpath = "//div[@class='mat-select-arrow-wrapper']")
    private WebElement buttonPag;

    @FindBy(xpath = "//input[@class='mat-input-element mat-form-field-autofill-control']")
    private WebElement filterText;

    @FindBy(xpath = "//button[@aria-label='Change sorting for firstName']")
    private WebElement headerFirstName;

    public void setEmployee()
    {
        CommonEvents.clickButton(tabEmployee);
    }

    public void navigateToEmployees()
    {
        CommonEvents.clickButton(tabEmployee);
        CommonEvents.pressEnterKey(tabEmployee.findElement(By.xpath("//button[contains(text(),'Employees')]")));
    }

    public void setTabEmployees()
    {
        WebElement tabEmployees = (new WebDriverWait(ManageDriver.getInstance().getWebDriver(), 30)).until(
                ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'Employee')]")));
        CommonEvents.pressEnterKey(tabEmployee.findElement(By.xpath("//button[contains(text(),'Employees')]")));
    }

    public void AddNewEmployee()
    {
        if (buttonAddNewEmployee != null)
        {
            CommonEvents.clickButton(buttonAddNewEmployee);
        }
    }

    public void deleteEmployeeModal()
    {
        if (deleteIcon != null)
        {
            CommonEvents.clickButton(deleteIcon);
        }
    }

    public void deleteEmployeeButton()
    {
        if (deleteButton != null)
        {
            CommonEvents.clickButton(deleteButton);
        }
    }

    public void sortFirstName()
    {
        if (headerFirstName != null)
        {
            CommonEvents.clickButton(headerFirstName);
        }
    }

    public void InsertDatos(DataTable datos)
    {
        if (datos != null)
        {
            List<List<String>> datosEmployeeList = datos.raw();
            for (List<String> emp : datosEmployeeList)
                switch (emp.get(0))
                {
                    case "firstName":
                        CommonEvents.setInputField(firstNameTextBox, emp.get(1));
                        break;
                    case "lastName":
                        CommonEvents.setInputField(lastNameTextBox, emp.get(1));
                        break;
                    case "address":
                        CommonEvents.setInputField(addressTextBox, emp.get(1));
                        break;
                    case "phone":
                        CommonEvents.setInputField(phoneTextBox, emp.get(1));
                        break;
                    case "email":
                        CommonEvents.setInputField(emailTextBox, emp.get(1));
                        break;
                    case "dni":
                        CommonEvents.setInputField(dniTextBox, emp.get(1));
                        break;
                    case "job Position":
                        CommonEvents.setInputField(jobPositionTextBox, emp.get(1));
                        break;
                }
        }
    }

    public void onClickSave()
    {
        if (buttonSave != null)
        {
            CommonEvents.clickButton(buttonSave);
        }
    }

    public void filterTextBox(String value)
    {
        if (filterText != null)
        {
            CommonEvents.setInputField(filterText, value);
        }
    }

    public void closeWindow()
    {
        waitForNextAction();
        webDriver.close();
    }

    public void waitForNextAction()
    {
        try
        {
            Thread.sleep(4000);
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void verifyRecord(String record) {

    }
}


